public with sharing class A17_LeaveRequestExample_Factory {

	private static Map<String, Map<String, Id>> recordTypes;
	private static Map<String, Map<String, Id>> recordTypesSOQL;

	static{
		recordTypesSOQL = A17_LeaveRequestExample_FactoryUtil.getRecordTypesFromSOQL();
		recordTypes = A17_LeaveRequestExample_FactoryUtil.getRecordTypesFromSchema();
	}
	

	public static Id getRecordTypeIdFromSchema(String objectName, String developerName){
		return A17_LeaveRequestExample_FactoryUtil.getRecordTypeId(
				A17_LeaveRequestExample_Factory.recordTypes,
				objectName,
				developerName
			);
	}

	public static Id getRecordTypeIdFromsSOQL(String objectName, String developerName){
		return A17_LeaveRequestExample_FactoryUtil.getRecordTypeId(
				A17_LeaveRequestExample_Factory.recordTypesSOQL,
				objectName,
				developerName
			);
	}
}

