public with sharing class A17_LeaveRequestExample_FactoryUtil {

	public static Map<String, Map<String, Id>> getRecordTypesFromSchema(){
		Map<String, Map<String, Id>> recordTypes = new Map<String, Map<String, Id>>();
		Map<String, Schema.SObjectType> description= Schema.getGlobalDescribe();
			for(String obj: description.keySet()) {
				Map<String, Id> objRecordTypes = new Map<String, Id>();
				for(Schema.RecordTypeInfo RecordTypeInfo:
					description.get(obj).getDescribe().getRecordTypeInfos()){

					String name = RecordTypeInfo.getName();
					Id recordTypeId = RecordTypeInfo.getRecordTypeId();

					objRecordTypes.put(name, recordTypeId);
				}
				recordTypes.put(obj, objRecordTypes);
			}
		return recordTypes;
	}


	public static Map<String, Map<String, Id>> getRecordTypesFromSOQL(){
		Map<String, Map<String, Id>> recordTypesSOQL = new Map<String, Map<String, Id>> ();
		for(RecordType recType:
			[SELECT Id, DeveloperName, SObjectType FROM RecordType]) {
			
			if(recordTypesSOQL.containsKey(recType.SObjectType)){
				Map<String, Id> records = recordTypesSOQL.get(recType.SObjectType);
				if(records.containsKey(recType.DeveloperName)){
					records.put(recType.DeveloperName, recType.Id);
				}
			}
			else{
					recordTypesSOQL.put(recType.SObjectType, new Map<String, Id>{recType.DeveloperName => recType.Id});
			}
		}
		return recordTypesSOQL;
	}

    public static Id getRecordTypeId(Map<String, Map<String, Id>> recordTypes, String sobjectName, String developerName) {
        if(recordTypes.containsKey(sobjectName)) {
            Map<String, Id> recordTypesByObject = recordTypes.get(sobjectName);
            if(recordTypesByObject.containsKey(developerName)) {
                return recordTypesByObject.get(developerName);
            } else {
                throw new A17_LeaveRequestExample_FactoryUtil.IncorrectDeveloperNameException();
            }
        } else {
            throw new A17_LeaveRequestExample_FactoryUtil.IncorrectSobjectNameException();
        }
    }

    public class IncorrectSobjectNameException extends Exception {}

    public class IncorrectDeveloperNameException extends Exception {}
}